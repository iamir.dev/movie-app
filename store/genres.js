export const state = () => ({
  genres: []
})
export const mutations = {
  SET_GENRES(state, genres) {
    state.genres = genres
  }
}

export const actions = {
  setGenres({commit}, genres) {
    commit('SET_GENRES', genres)
  }
}
