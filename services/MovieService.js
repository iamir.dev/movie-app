import {api, API_DEFAULT_PARAMS} from './Api'

export default {
  getMovies(page, releaseStartDate = null, releaseEndDate = null) {
    let params = {
      ...API_DEFAULT_PARAMS,
      sort_by: `release_date.desc`,
      include_adult: false,
      include_video: false,
      page,
      with_watch_monetization_types: `flatrate`
    }
    if (releaseStartDate && releaseEndDate) {
      params = {
        ...params,
        'release_date.gte': releaseStartDate,
        'release_date.lte': releaseEndDate
      }
    }
    return api.get(`/discover/movie`, {
      params,
    })
  },
  getGenres() {
    const params = {
      ...API_DEFAULT_PARAMS,
    }
    return api.get(`/genre/movie/list`, {
      params,
    })
  },
  getMovie(id) {
    const params = {
      ...API_DEFAULT_PARAMS,
    }
    return api.get(`movie/${id}`, {
      params,
    })
  },
  getCredit(id) {
    const params = {
      ...API_DEFAULT_PARAMS,
    }
    return api.get(`movie/${id}/credits`, {
      params,
    })
  }
}
