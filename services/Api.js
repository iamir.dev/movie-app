import axios from 'axios'

const API_DEFAULT_PARAMS = {
  api_key: `${process.env.key}`,
  language: `en-US`,
}
const api = axios.create({
  baseURL: `https://api.themoviedb.org/3`,
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
})
export {api, API_DEFAULT_PARAMS}
